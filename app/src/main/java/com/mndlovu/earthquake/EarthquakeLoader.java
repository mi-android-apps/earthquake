package com.mndlovu.earthquake;

import android.content.AsyncTaskLoader;
import android.content.Context;

import java.util.List;

/**
 * Loads a list of earthquakes by using an AsyncTask to perform the
 * network request to the given URL.
 */
public class EarthquakeLoader extends AsyncTaskLoader<List<Earthquake>> {

    /** Tag for log messages */
    private static final String LOG_TAG = EarthquakeLoader.class.getName();

    /** Query URL */
    private String mUrl;

    /**
     * Constructs a new {@link EarthquakeLoader}.
     *
     * @param context of the activity
     * @param url to load data from
     */
    public EarthquakeLoader(Context context, String url) {
        super(context);
        System.out.println("========== EarthquakeLoader contractor =============");
        mUrl = url;
    }

    @Override
    protected void onStartLoading() {
        System.out.println("-------------------[start] onStartLoading --------------------------");
        forceLoad();
        System.out.println("-------------------[end] onStartLoading --------------------------");
    }

    /**
     * This is on a background thread.
     */
    @Override
    public List<Earthquake> loadInBackground() {
        System.out.println("----------------[start] loadInBackground ---------------------");
        if (mUrl == null) {
            return null;
        }

        // Perform the network request, parse the response, and extract a list of earthquakes.
        List<Earthquake> earthquakes = QueryUtils.fetchEarthquakeData(mUrl);
        System.out.println(earthquakes);
        System.out.println("----------------------[end] loadInBackground -------------------");
        return earthquakes;
    }
}
