# EarthQuake

This app displays a list of recent earthquakes in the world from the U.S. Geological Survey (USGS) organization.
More info on the USGS Earthquake API available at: https://earthquake.usgs.gov/fdsnws/event/1/

### Objectives
* Learn how to parse JSON, one of the most useful data formats.
* Learn how to connect to the internet in your Android code.
* Threads to use threads to make your device do multiple things at once.
### Build Instructions
This sample uses the Gradle build system. To build this project, use the
"gradlew build" command or use "Import Project" in Android Studio.

### screenshots
![please find images under app-screenshots directory](app-screenshots/screen-1.png "screen one")